#!/usr/bin/env python
# -*- coding: utf8 -*-

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#


import sys, re, argparse, requests, json

parser = argparse.ArgumentParser(description = 'Assigne le numéro de chaîne freeboxtv au chaîne tvheadend 4.0')

parser.add_argument('--hts-url', default = 'http://localhost:9981/', help = 'url vers le serveur hts (http://localhost:9981 par défaut)')
parser.add_argument('--hts-user', default = 'admin', help = 'nom d\'utilisateur hts (admin par défaut)')
parser.add_argument('--hts-password', default = 'admin', help = 'mot de passe hts (admin par défaut)')
parser.add_argument('--playlist-url', default = 'http://mafreebox.freebox.fr/freeboxtv/playlist.m3u', help = 'url vers la playlist (http://mafreebox.freebox.fr/freeboxtv/playlist.m3u par défaut)')

args = parser.parse_args()



pattern = re.compile("^#EXTINF:0,([0-9]+) - (.*?)(?: HD$| \(.*\)$|$)")
m3u = {}
r = requests.get(args.playlist_url)
if r.ok :
    for l in r.iter_lines(decode_unicode = True) :
        if l.startswith('#EXTINF'):
            match = pattern.search(l)
        elif l.startswith('rtsp'):
            m3u[match.group(2).lower()] = match.group(1)
else :
    print "Erreur, le téléchargement de la playlist a échoué"
    sys.exit(1)

r = requests.post(
    args.hts_url+'/api/channel/grid',
    auth=requests.auth.HTTPDigestAuth(args.hts_user, args.hts_password),
    data={'limit':100000}
  )
if r.ok :
    print r.json()
    for chan in r.json()['entries'] :
#        print 'Recherche no de %s'%chan['name'] FIXEME https://diasp.eu/posts/6036135#9cd7c610931a01357a244061862b8e7b
        if m3u.has_key(chan['name'].lower()) :
            print '  - %s'%m3u[chan['name'].lower()]
            r = requests.post(
                args.hts_url+'/api/idnode/save',
                auth=requests.auth.HTTPDigestAuth(args.hts_user, args.hts_password),
                data={
                    'node': json.dumps({
                        "number":m3u[chan['name'].lower()],
                        "uuid":chan['uuid']
                      })
                  }
              )
            if r.ok :
                print '  - sauver'
            else :
                print '  - erreur'
        else :
            print '  - no found'
else :
    print "Erreur, le téléchargement de la liste de chaînes a échoué"
